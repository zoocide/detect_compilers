@echo off
setlocal enabledelayedexpansion
echo @echo off
call :set_vs_vars
call :set_ifort_vars
endlocal
goto :eof


:set_vs_vars
call :get_tool_vers vers VS 20 -1 10 0COMNTOOLS
if "%vers%" == "" echo rem Visual Studio is not detected& exit /b 1
echo rem set Visual Studio environment
call :head latest %vers%
call :tail others %vers%
call :reverse others %others%
for %%v in (%others%) do echo rem call "!VS%%v0COMNTOOLS!vsvars32.bat"
echo call "!VS%latest%0COMNTOOLS!vsvars32.bat"
exit /b 0

:set_ifort_vars
call :get_tool_vers vers IFORT_COMPILER 20 -1 5
if "%vers%" == "" echo rem Intel Fortran is not detected& exit /b 1
echo rem set Intel Fortran environment
echo rem alternative parameter ia32 intel64
call :head latest %vers%
call :tail others %vers%
call :reverse others %others%
for %%v in (%others%) do echo rem call "!IFORT_COMPILER%%v!bin\ifortvars.bat" ia32
echo call "!IFORT_COMPILER%latest%!bin\ifortvars.bat" ia32
exit /b 0

rem call :detect_var result_var_name var_name_prefix start step end var_name_postfix
rem                  1               2               3     4    5   6
:detect_var
set %1=
for /L %%i in (%3, %4, %5) do (
  if defined %2%%i%6 set %1=!%2%%i%6!&exit /b 0
)
exit /b 1

rem call :get_tool_vers result_var_name var_name_prefix start step end var_name_postfix
rem                  1               2               3     4    5   6
:get_tool_vers
set %1=
for /L %%i in (%3, %4, %5) do (
  if defined %2%%i%6 set %1=!%1! %%i
)
exit /b 0

rem call :head res_var_name list
:head
set %1=%2
exit /b 0

rem call :tail res_var_name list
:tail
setlocal enabledelayedexpansion
set vname=%1
set tail=%3
shift & shift & shift
:tail__loop
if "%1" == "" goto tail__loop_end
set tail=%tail% %1
shift
goto tail__loop
:tail__loop_end
endlocal & set %vname%=%tail%
exit /b 0

rem call :reverse res_var_name list
:reverse
setlocal
set vname=%1
set res=%2
shift & shift
:reverse__loop
if "%1" == "" goto reverse__loop_end
set res=%1 %res%
shift
goto reverse__loop
:reverse__loop_end
endlocal & set %vname%=%res%
exit /b 0